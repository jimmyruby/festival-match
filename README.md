# Festival Match
This project is a website that matches the Spotify artists you listen to most with upcoming festivals that they will be performing at. The site uses a React frontend with an Express backend and is hosted via AWS Amplify.

You can find the production version of the site [here](https://master.d1eozsmgi5hb3t.amplifyapp.com/)