import os
import json
from bs4 import BeautifulSoup
from pathlib import Path

file_list = os.listdir("festivals_html")
lineup_file = 'lineup_list.json'
lineup_list = []
recorded_festivals = []
if os.path.exists(lineup_file):
    with open(lineup_file, 'r') as f:
        if f and os.stat(lineup_file).st_size > 0:
            lineup_list = json.load(f)
        recorded_festivals = list(map(lambda x: x["festival"], lineup_list))

for festival in file_list:
    if festival in recorded_festivals:
        print("Already recorded lineup for " + festival)
        continue

    if ".html" in festival:
        html_doc = Path("festivals_html/" + festival).read_text()
        soup = BeautifulSoup(html_doc, 'html.parser')
        lineup = soup.find("div", {"class": "lineup"})
        if lineup is not None:
            row = {
                "source_file": festival,
                "name": festival.split(".")[0],
                "image": festival.split(".")[0] + ".jpg",
                "lineup": list(map(lambda x: x.text, lineup.findAll("a")))
            }
            lineup_list.append(row)
            print("Got lineup for " + festival)
        else:
            print("Could not parse lineup for " + festival)

with open(lineup_file, 'w+') as f:
    json.dump(lineup_list, f)