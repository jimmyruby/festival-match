'''
Imports
'''
from bs4 import BeautifulSoup
import os
from pathlib import Path

'''
Get list of festivals
'''
curl 'https://www.nocturnalwonderland.com/lineup/' \
  -H 'authority: www.nocturnalwonderland.com' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'cache-control: max-age=0' \
  -H 'cookie: pys_landing_page=https://www.nocturnalwonderland.com/; AWSELB=7DBFB92F0E89EDCBB192D94BFDACED0424F898672602919AF05B9D4FA58EAE5EB28A64AA5D9A73DB0DB871BE937D62C7F968C94B819E3D3567E1DDCC15626CF61F34946331; wordpress_google_apps_login=b982cffff2c7308b0d8fa46b4a8b6e0e; OptanonConsent=isIABGlobal=false&datestamp=Tue+Sep+06+2022+22%3A25%3A49+GMT-0400+(Eastern+Daylight+Time)&version=5.11.0&landingPath=NotLandingPage&groups=1%3A1%2C2%3A1%2C3%3A1%2C4%3A1%2C0_236034%3A1%2C0_236035%3A1%2C0_236036%3A1%2C0_236037%3A1%2C0_236038%3A1%2C0_236039%3A1%2C0_236040%3A1%2C0_236041%3A1%2C0_236042%3A1%2C0_236043%3A1&AwaitingReconsent=false' \
  -H 'sec-ch-ua: "Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "macOS"' \
  -H 'sec-fetch-dest: document' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-site: none' \
  -H 'sec-fetch-user: ?1' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' \
  --compressed
request = "'{0}' \
  -H 'authority: www.nocturnalwonderland.com' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'cache-control: max-age=0' \
  -H 'cookie: pys_landing_page={0}; wordpress_google_apps_login=b9bc2c8033ea0a9de29a4091ec387184; OptanonConsent=isIABGlobal=false&datestamp=Mon+Sep+05+2022+20%3A20%3A38+GMT-0400+(Eastern+Daylight+Time)&version=5.11.0&landingPath=NotLandingPage&groups=1%3A1%2C2%3A1%2C3%3A1%2C4%3A1%2C0_236034%3A1%2C0_236035%3A1%2C0_236036%3A1%2C0_236037%3A1%2C0_236038%3A1%2C0_236039%3A1%2C0_236040%3A1%2C0_236041%3A1%2C0_236042%3A1%2C0_236043%3A1&AwaitingReconsent=false' \
  -H 'sec-ch-ua: \"Chromium\";v=\"104\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"104\"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: \"macOS\"' \
  -H 'sec-fetch-dest: document' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-site: none' \
  -H 'sec-fetch-user: ?1' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' \
  --compressed"

festivals_url = "https://www.insomniac.com/events/festivals/"
if not os.path.exists("festivals.html"):
    os.system(f"curl {festivals_url} > festivals.html")
festivals_page = Path('festivals.html').read_text()
soup = BeautifulSoup(festivals_page, 'html.parser')
festival_blocks = soup.findAll("div", {"class": "card__content"})
festival_urls = []
for block in festival_blocks:
    festival_urls.append(block.find("a")["href"] + "lineup")

'''
Get lineup for festival
'''
for url in festival_urls:
    festival_file = "festival_html/" + url.split("/")[2].split(".")[1] + ".html"
    print(festival_file, url)
    if not os.path.exists(festival_file):
        command = f"curl {request.format(url)} > {festival_file}"
        print(command)
        os.system(command)
    html_doc = Path(festival_file).read_text()
    print(len(html_doc))
    soup = BeautifulSoup(html_doc, 'html.parser')
    artist_tags = soup.find("div", {"class": "lineup"}).findAll("a")
# for tag in artist_tags:
#     print(tag.text)