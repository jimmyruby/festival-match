import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Login from './components/login';
import Matches from './components/matches';

class App extends Component {
    constructor(props){
        super(props);

    }
    render() {
        return (
        <Router>
            <Routes>
                    <Route exact path='/' element={< Login />}></Route>
                    <Route exact path='/match' element={< Matches />}></Route>
                    <Route exact path='/login' element={< Login />}></Route>
            </Routes>
        </Router>
    );
    }
}

export default App;
