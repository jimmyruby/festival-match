import React, { Component } from 'react';
import '../css/festival-card.css';
import 'aos/dist/aos.css';
import Aos from 'aos';
// import LazyLoadImage from 'react-lazy-load-image-component';

class FestivalCard extends Component {

    constructor(props) {
        super(props);
        this.index = this.props.index;
        this.festival = this.props.festival;
        this.imageContainer = React.createRef(null);
        this.imageContainerScroll = 0;
        this.scrollScalar = 3;
        this.clickCard = this.clickCard.bind(this);
        this.getColor = this.getColor.bind(this);
        // this.getImages = this.getImages.bind(this);
        this.handlePageScroll = this.handlePageScroll.bind(this);
        this.handleContainerScroll = this.handleContainerScroll.bind(this);
        this.currentColor = this.festival.colors[0]
        this.state = {
            style: {
                backgroundImage: `linear-gradient(rgb(${this.currentColor}) 83%, rgb(${this.festival.next_color}))`
            }
        }
    }

    componentDidMount() {
        // this.getImages();
        Aos.init({ duration: 1000 });
        window.addEventListener('scroll', this.handlePageScroll);
        const scroll = document.getElementById('image-container-' + this.index);
        this.containerTop = document.getElementById('festival-card-' + this.index).getBoundingClientRect().top;
        this.containerHeight = document.getElementById('festival-card-0').getBoundingClientRect().bottom;
        scroll.addEventListener('scroll', this.handleContainerScroll);
        this.getColor(this.festival.colors[0], this.festival.colors[0])
    }

    async getImages(uri, fest){
        for(let image of this.festival.images) {
            console.log(image)
            const imageUrl = await Storage.get(`image/${uri}`, {level: 'private'});
            let state = this.state;
            state[image] = imageUrl;
            this.setState(state);
            console.log(state);
        }
    }

    getColor(color_1, color_2, i) {
        if(!color_2) {
            this.setState({
                style: {
                    backgroundImage: `linear-gradient(rgb(${color_1}) 83%, rgb(${this.festival.next_color}))`
                }
            })
            return
        }
        const ratio = (Math.ceil(i) - i);
        this.currentColor = [
            color_2[0] + (color_1[0] - color_2[0]) * ratio,
            color_2[1] + (color_1[1] - color_2[1]) * ratio,
            color_2[2] + (color_1[2] - color_2[2]) * ratio
        ]

        this.setState({
            style: {
                backgroundImage: `linear-gradient(rgb(${this.currentColor}) 83%, rgb(${this.festival.next_color}))`
            }
        })
    }

    handlePageScroll(event) {
        const scroll = window.scrollY;
        if(this.festival.colors.length > 0){
            const relativeScroll = scroll - this.containerTop;
            this.imageContainerScroll = relativeScroll * this.scrollScalar;
            // let imageID = (this.festival.images.length - ((this.containerHeight - relativeScroll) / this.containerHeight) * this.festival.images.length) * 2;
            // if(imageID >= this.festival.images.length) {
            //     imageID = this.festival.images.length - 1;
            // } else if (imageID < 0) {
            //     imageID = 0;
            // } else {
            //     this.getColor(this.festival.colors[Math.floor(imageID)], this.festival.colors[Math.ceil(imageID)], imageID);
            // }

            const element = document.getElementById('image-container-' + this.index);
            if(element){
                element.scrollTo({
                    top: 0,
                    left: this.imageContainerScroll
                });
            }
        }
    }

    handleContainerScroll(event) {
        const element = document.getElementById('image-container-' + this.index);
        if(element){
            element.scrollTo({
                top: 0,
                left: this.imageContainerScroll
            });
        }
    }

    clickCard () {
        window.open(this.festival.link);
    }

    render() {
        this.first = [];
        this.second = [];
        this.third = [];
        var self = this;
        const len = self.festival.artists.length;
        this.festival.artists.map((festival, i) => {
            if(len < 4 || len * 0.1 > i) {
                self.first.push(festival);
            } else if (len * 0.3 > i) {
                self.second.push(festival);
            } else {
                self.third.push(festival);
            }
        })
        return (
            <div className='festival-card' id={"festival-card-" + this.index} style={this.state.style}>
                {/* <div className={this.festival.connectorType}></div>
                <div className='date-bubble'>Dec 20 2022</div> */}
                <div style={{display: "flex", justifyContent: "space-between"}}>
                    <h1 data-aos="fade-up" className='title' style={{fontSize: "6vw"}} onClick={this.clickCard}>{this.festival.name}</h1>
                    <h1 data-aos="fade-up" className='date' style={{fontSize: "3vw", display: "flex", marginBottom: "10px", alignItems: "flex-end"}}>{this.festival.date}</h1>
                </div>
                <div id={"image-container-" + this.index} className="image-container" ref={this.imageContainer} style={{height: "50vh", paddingTop: "1vh", display: "flex", flexDirection: "row", overflowX: "scroll"}}>
                    {
                        this.festival.images.map((image) => { return (
                                <div className='festival-img' onClick={this.clickCard}>
                                    {/* <div className='full-lineup' style={{backgroundColor: "rgba(0, 0, 0, 0.3)", borderRadius: "15px", height: "50%", width: "60%", position: "absolute", zIndex: "1", textAlign: "center", display: "flex", verticalAlign: "middle"}}><p style={{margin: "auto"}}>Full Lineup</p></div> */}
                                    {/* <LazyLoadImage src={image} /> */}
                                    <img src={image} height="100%" width="100%" style={{borderRadius: "15px", aspectRatio: 1, objectFit: "cover"}}></img>
                                </div> 
                            )
                        })
                    }
                </div>

                <div style={{display: "flex", flexDirection: "column", marginTop: "10px"}}>
                    <div data-aos="fade-down" className='artist-bubble first'>
                        {
                            this.first.map((artist, i) => {
                                if(i < this.first.length - 1) {
                                    return artist + " • ";
                                } else {
                                    return artist;
                                }
                            })
                        }
                    </div>
                    <div data-aos="fade-down" className='artist-bubble second'>
                        {
                            this.second.map((artist, i) => {
                                if(i < this.second.length - 1) {
                                    return artist + " • ";
                                } else {
                                    return artist;
                                }
                            })
                        }                    
                    </div>
                    <div data-aos="fade-down" className='artist-bubble third'>
                        {
                            this.third.map((artist, i) => {
                                if(i < this.third.length - 1) {
                                    return artist + " • ";
                                } else {
                                    return artist;
                                }
                            })
                        }      
                    </div>                                
                </div>
            </div>
        )
    }
}

export default FestivalCard;