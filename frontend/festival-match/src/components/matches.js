import React, { Component } from 'react'
import { getAccessToken, getLineupList, getTopArtists, FRONTEND_URL } from '../services/ApiService';
import FestivalCard from './festival-card';
import '../css/matches.css';

class Matches extends Component {
    constructor(props){
        super(props)
    
        // Set initial state
        this.loading_strings = [
            "Building the main stage", 
            "Turning up the speakers", 
            "Testing the light show", 
            "Loading the fireworks",
            "Designing the drone show",
            "Making the totem"
        ];
        this.state = {
            "festivals": [], 
            "has_data": false, 
            loading_string: this.loading_strings[0]
        }
        this.startLoadingStrings = this.startLoadingStrings.bind(this);
        this.checkAccessToken = this.checkAccessToken.bind(this);
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        // Auth.currentCredentials()
        // console.log(Storage.list(""));
        // console.log(Storage.list("/images"));
        // console.log(Storage.get("/images/boo-halloween.jpg"));
        this.startLoadingStrings();
        this.checkAccessToken();
    }

    startLoadingStrings() {
        setInterval(function() {
            const index = Math.floor(this.loading_strings.length * Math.random());
            this.setState({
                startStringIndex: index,
                loading_string: this.loading_strings[index]
            })
        }.bind(this), 1500);
    }

    checkAccessToken() {
        if(!document.cookie.includes("access_token")) {
            const urlParams = new URLSearchParams(window.location.search);
            var code = urlParams.get('code');
            if (code != null) {
                getAccessToken(code).then((result) => {
                    this.getData();
                });
            }
        } else if(this.state.festivals.length == 0){
            this.getData();
        }
    }

    getData() {
        getTopArtists('short_term').then((result) => {
            console.log(result)
            if(!result.error) {
                getLineupList(result).then((result) => {
                    this.setState({festivals: result, has_data: true});
                })
            } else {
                console.error(result.error);
            }
        });
    }
	
    render(){
        if (this.state.has_data) {
            return (
                <div className='festival-card-container'>
                {
                    this.state.festivals.map((festival, i) => {
                        if (i == 0) {
                            festival.connectorType = 'top-connector';
                            festival.next_color = this.state.festivals[i + 1].colors[0];
                        } else if (i == this.state.festivals.length - 1) {
                            festival.next_color = festival.colors[0];
                            festival.connectorType = 'bottom-connector';
                        } else {
                            festival.next_color = this.state.festivals[i + 1].colors[0];
                            festival.connectorType = 'middle-connector';
                        }
                        return (<FestivalCard festival={festival} index={i}/>)
                    })
                }
                </div>
            )
        } else {
            return (
                <div id="loading-container">
                    <img id="loading-image" src={`${FRONTEND_URL}/loading-2.gif`} height="150px" width="150px"></img>
                    <h1 id="loading-text">{this.state.loading_string}</h1>
                </div>
            )
        }
    }
}

export default Matches;
