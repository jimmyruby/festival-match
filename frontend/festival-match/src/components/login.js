import React, { Component } from 'react'
import '../css/login.css';
import { FRONTEND_URL } from '../services/ApiService';
import video from '../assets/edc.mp4';
// import spotify from '../assets/spotify.png';

class Login extends Component {
    
    constructor(props){
        super(props)
    }

    componentDidMount() {
        if (document.cookie.includes("access_token")) {
            window.location.href = `${FRONTEND_URL}/match/`
        }
    }

    login() {
        window.location.href = "https://accounts.spotify.com/authorize?client_id=4d7a07ec983d4e28b31a12b6dfc31026&response_type=code&redirect_uri=" + encodeURI(`${FRONTEND_URL}/match/`) + "&show_dialog=true&scope=user-top-read user-read-playback-state" 
    }
	
    render(){
        return (
            <div className="login-container">
                <div>
                    <video autoPlay loop muted>
                        <source src={video} type="video/mp4"/>
                    </video>
                </div>
                <h1 style={{marginTop: "5vh"}}>Festival Match</h1>
                <p id="description">Festival Match finds upcoming music festivals with your top Spotify artists on the lineup. Click the button below to get started!</p>
                <button className='login-button' onClick={this.login}>
                    <p>Connect Spotify</p>
                    <img id='spotify-icon' src='spotify.png'></img>
                </button>
            </div>
        )
    }
}

export default Login;