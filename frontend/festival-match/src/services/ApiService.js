import { API, Amplify } from 'aws-amplify';
import awsExports from "../aws-exports";
Amplify.configure(awsExports);

let requesting = false;
// export const FRONTEND_URL = "http://localhost:4200"
export const FRONTEND_URL = "https://master.d1eozsmgi5hb3t.amplifyapp.com"
export const BACKEND_URL  = "https://6t52ktemsh.execute-api.us-east-1.amazonaws.com/dev"

function handleError(result) {
    console.error(result);
    try {
        if (result.error.message){
            if (result.error.message.includes("The access token expired")) {
                document.cookie = "access_token=;expires=" + new Date(0).toUTCString()
                // window.location.href = FRONTEND_URL;
            }
        } else if (result.error_description) {
            console.error(result.error_description);
            // window.location.href = FRONTEND_URL;
        }
    } catch(error) {}
}

export async function getAccessToken(code) {
    try {
        let requesting = false;
        if(!requesting) {
            requesting = true;
            const body = {body: {code: code}}
            const res = await API.post('myapi', `/login`, body)
                .then((data) => {
                    requesting = false;
                    if(!data.error) {
                        const expires = new Date();
                        expires.setHours(expires.getHours() + 1); // token expires in 1 hour, set the expiration timestamp
                        document.cookie = "access_token=" + data.access_token + ";expires=" + expires.toUTCString();
                        return data.access_token;
                    } else {
                        handleError(data);
                    }
                }
            );
        }
    } catch(error) {
        handleError(error);
    }
}

export async function getTopArtists(time_range) {
    const body = {body: {access_token: document.cookie.split("=")[1], time_range: time_range}}
    console.log(body)
    return await API.post('myapi', `/artists`, body)
        .then((result) => {
        console.log(result)
        if(!result.error) {
            return result.items;
        } else {
            handleError(result);
            return result;
        }
    });
}

export async function getLineupList(artists) {
    const artist_names = artists.map((x) => x.name);
    console.log(artist_names)
    const body = {body: {access_token: document.cookie.split("=")[1], artist_names: artist_names}}
    return await API.post('myapi', '/get-matching-festivals', body).then((result) => {
        return result;
    });
}

