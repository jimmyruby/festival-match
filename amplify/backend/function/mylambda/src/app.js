/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/




const express = require('express')
var request = require('request')
const bodyParser = require('body-parser')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const fs = require('fs'), path = require('path');
var jsonParser = bodyParser.json()

// declare a new express app
const app = express()
app.use(function (req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");

    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())
app.use(express.static('data/images'))

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});

const getColors = require('get-image-colors');

const IMAGE_BUCKET_URL = "https://amplify-festivalmatch-dev-211606-deployment.s3.amazonaws.com/images/"
global.client_secret = fs.readFileSync('data/client_secret.txt', { encoding: 'utf-8' });
global.festival_list = JSON.parse(fs.readFileSync('data/lineup_list.json', { encoding: 'utf-8' }));

app.post('/login', function (req, res) {
    apiURL = 'https://api.spotify.com/v1/me';
    const client_id = '4d7a07ec983d4e28b31a12b6dfc31026'; // Your client id
    const redirect_uri = encodeURI("https://master.d1eozsmgi5hb3t.amplifyapp.com/match/"); // Your redirect uri
    const scope = 'user-top-read user-read-playback-state';
    const url = '&grant_type=authorization_code' +
        '&scope=' + scope +
        '&code=' + req.body.code +
        '&redirect_uri=' + redirect_uri +
        '&client_id=' + client_id +
        '&client_secret=' + client_secret;


    fetch('https://accounts.spotify.com/api/token', {
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + Buffer.from(client_id + ':' + client_secret).toString('base64')
        },
        body: url
    }).then((result) => {
        return result.json();
    }).then((result) => {
        res.json(result);
    });
});

app.post('/artists', function (req, res) {
    fetch(`https://api.spotify.com/v1/me/top/artists?limit=50&time_range=${req.body.time_range}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + req.body.access_token
        }
    }).then((result) => {
        return result.json();
    }).then((result) => {
        res.json(result);
    });
});

async function addColors(festivals) {
    for(let festival of festivals){
        festival.colors = [];
        for(let image of festival.images){
            const colors = await getColors(path.join(__dirname, 'data/images/' + image));
            // let brightest_color = colors[0];
            // let top_brightness = 0;
            // for(let color of colors) {
            //     brightness = color._rgb[0] + color._rgb[1] + color._rgb[2];
            //     if (brightness > top_brightness) {
            //         brightest_color = color;
            //         top_brightness = brightness;
            //     }
            // }
            // const selected = brightest_color._rgb.slice(0, 3);
            const selected = colors[0]._rgb.slice(0, 3);
            festival.colors.push(selected);
            // festival.colors.push(`rgb(${selected[0]}, ${selected[1]}, ${selected[2]})`);
        }
    }
    return festivals
}
app.post('/get-matching-festivals', jsonParser, async function (req, res) {
    const festivals = [];
    for (let festival of festival_list) {
        if (new Date(festival.date) < new Date()) {
            continue; // skip festival if it's in the past
        }
        festival.artists = [];
        for (let artist of req.body.artist_names) {
            if (festival.lineup.includes(artist)) {
                festival.artists.push(artist);
            }
        }
        delete festival.lineup; // we don't need the full lineup, only the matching artists
        if (festival.artists.length > 0) {
            festival.colors = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
            for (let i = 0; i < 3; i++) {
                for (let j = 0; j < 3; j++) {
                    festival.colors[i][j] = Math.floor(30 + Math.random() * 206);
                }
            }
            festivals.push(festival);
        }
        for(let i = 0; i < festival.images.length; i++){
            festival.images[i] = IMAGE_BUCKET_URL + festival.images[i]
        }
    }
    festivals.sort((a, b) => { // sort festivals by date
        return new Date(a.date).getTime() - new Date(b.date).getTime();
    });
    res.json(festivals);
    // addColors(festivals).then((result) => {
    //     res.json(result);
    // })
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
